<?php

use PHPUnit\Framework\TestCase;

require_once 'challengetwo_sort.php';
require_once 'challengetwo_assertions.php';

class SortTestData{

  public array $idScorePairListUnsorted;
  public array $idScorePairListSorted;

  public array $expectedIdScorePairListSorted;
  public array $expectedIdScorePairListSortedLength5;

  public function __construct() {
    $this->idScorePairListUnsorted = array(
      new Pair(1, 0),
      new Pair(2, 1),
      new Pair(3, -3),
      new Pair(4, 1),
      new Pair(5, -5),
      new Pair(6, 1.8333333333333333),
      new Pair(7, 1.5),
      new Pair(8, 1)
    );

    $this->idScorePairListSorted = array(
      new Pair(6, 1.8333333333333333),
      new Pair(7, 1.5),
      new Pair(2, 1),
      new Pair(4, 1),
      new Pair(8, 1),
      new Pair(1, 0),
      new Pair(3, -3),
      new Pair(5, -5)
    );

    $this->expectedIdScorePairListSorted = array(
      new Pair(6, 1.8333333333333333),
      new Pair(7, 1.5),
      new Pair(2, 1),
      new Pair(4, 1),
      new Pair(8, 1),
      new Pair(1, 0),
      new Pair(3, -3),
      new Pair(5, -5)
    );

    $this->expectedIdScorePairListSortedLength05 = array(
      new Pair(6, 1.8333333333333333),
      new Pair(7, 1.5),
      new Pair(2, 1),
      new Pair(4, 1),
      new Pair(8, 1)
    );
  }
}

class test_challengetwo_sort extends TestCase {

  public function idScorePairListUnsortedDataProvider() {
    $testData = new SortTestData();

    return [
      [ $testData->idScorePairListUnsorted,  0, false, $testData->idScorePairListUnsorted ], // no sort takes place
      [ $testData->idScorePairListUnsorted, -1, true,  $testData->expectedIdScorePairListSorted ],
      [ $testData->idScorePairListUnsorted,  5, true,  $testData->expectedIdScorePairListSortedLength05 ]
    ];
  }

  public function idScorePairListSortedDataProvider() {
    $testData = new SortTestData();

    return [
      [ $testData->idScorePairListSorted,  0, false, $testData->idScorePairListSorted ], // no sort takes place
      [ $testData->idScorePairListSorted, -1, true,  $testData->expectedIdScorePairListSorted ],
      [ $testData->idScorePairListSorted,  5, true,  $testData->expectedIdScorePairListSortedLength05 ]
    ];
  }

  /**
  * @test
  * @dataProvider idScorePairListUnsortedDataProvider
  */
  public function testSortUnsortedScore($idScorePairList, $maxElements, $expectedHasBeenSorted, $expectedSortedList) {
    $hasBeenSorted = sort_relationship_score_highest_to_lowest($idScorePairList, $maxElements);

    $this->assertequals($expectedHasBeenSorted, $hasBeenSorted);
    PairAssert::assertSamePairList($expectedSortedList, $idScorePairList);
  }

  /**
  * @test
  * @dataProvider idScorePairListSortedDataProvider
  */
  public function testSortSortedScore($idScorePairList, $maxElements, $expectedHasBeenSorted, $expectedSortedList) {
    $hasBeenSorted = sort_relationship_score_highest_to_lowest($idScorePairList, $maxElements);

    $this->assertequals($expectedHasBeenSorted, $hasBeenSorted);
    PairAssert::assertSamePairList($expectedSortedList, $idScorePairList);
  }

};

?>
