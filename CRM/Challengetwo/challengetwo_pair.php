<?php

/* a simple class that represents a pair of arbitrary values.
*/
class Pair{
  public function __construct (public mixed $first, public mixed $second) {}
};

?>
