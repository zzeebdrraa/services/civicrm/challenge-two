<?php
use CRM_Challengetwo_ExtensionUtil as E;

class CRM_Challengetwo_Page_Networking extends CRM_Core_Page {

  protected const MAX_DEPTH_FOR_SCORE_CALCULATION = 10;
  protected const NUM_SCORES_TO_DISPLAY = 10;

  public function run() {
    CRM_Utils_System::setTitle(E::ts('Networking'));

    $contactsNameMap = array();
    $contactsRelMap = array();

    query_individual_contacts_api_v3($contactsRelMap, $contactsNameMap);

    $idScorePairList = calculate_relationship_score($contactsRelMap, self::MAX_DEPTH_FOR_SCORE_CALCULATION);
    $sortOk = sort_relationship_score_highest_to_lowest($idScorePairList, self::NUM_SCORES_TO_DISPLAY);

    $outputList = array();
    if ( $sortOk ) {
      $outputList = generate_output_list($contactsNameMap, $idScorePairList);
    } else {
      Civi::log()->error('could not sort relationship score. list of calculated scores might be null.');
    }

    // Assign a variable for use in a template
    $this->assign('outputList', $outputList);

    parent::run();
  }
}
