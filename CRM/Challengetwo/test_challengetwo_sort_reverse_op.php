<?php

use PHPUnit\Framework\TestCase;

require_once 'challengetwo_sort.php';
require_once 'challengetwo_pair.php';

class test_challengetwo_sort_reverse_op extends TestCase {

  public function integerDataProvider() {
    return [
      // a   b  sortflag
      [  0,  0,  0 ],
      [  1,  0, -1 ],
      [  0,  1,  1 ],
      [  0, -1, -1 ],
      [ -1,  0,  1 ],
      [ -1, -1,  0 ],
      [  1, -1, -1 ],
      [ -1,  1,  1 ],
    ];
  }

  public function floatDataProvider() {
    return [
      // a     b   sortflag
      [ 0.0,  0.0,  0 ],
      [ 1.1,  0.0, -1 ],
      [ 0.0,  1.1,  1 ],
      [ 0.0, -1.1, -1 ],
      [-1.1,  0.0,  1 ],
      [-1.1, -1.1,  0 ],
      [ 1.1, -1.1, -1 ],
      [-1.1,  1.1,  1 ],
      [-1.1, -1.2, -1 ],
      [-1.2, -1.1,  1 ],
      [ 1.1,  1.2,  1 ],
      [ 1.2,  1.1, -1 ],
    ];
  }

  /**
  * @test
  * @dataProvider integerDataProvider
  * @dataProvider floatDataProvider
  */
  public function testSortPair($valA, $valB, $expectedResult) {
    $result = reversePairSortOp(new Pair(0, $valA), new Pair(0, $valB));
    $this->assertEquals($expectedResult, $result);
  }
}

?>
