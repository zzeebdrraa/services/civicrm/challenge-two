<?php

require_once 'challengetwo_pair.php';

/*
  @param $a a value pair
  @param $b a value pair

  @return 0 the second values of each pair are equal
  @return 1 the second value of $pairA is smaller than the second value of $pairB
  @return -1 the second value of $pairA is greater than the second value of $pairB
*/
function reversePairSortOp(Pair $pairA, Pair $pairB): int {
  if ( $pairA->second == $pairB->second ) return 0;
  if ( $pairA->second < $pairB->second ) return 1;
  return -1;
}

/*
  @param &$idScorePairListRef

    a list that contains value pairs as elements. a pair is a object of type Pair.

    the second value of a pair is expected to be a number. the first value
    can be of any type and value.

    sorting is done on the second value of each pair.

  @param $maxHighestValues

    if set to 0

      output-list will be empty, no processing will be performed at all

    if set to a value < 0

      output-list wont be truncated. it will contain the same number of
      elements as $idScorePairListRef

    if set to a value > 0

      output-list will be truncated to the number of elements specified.
      the returned values are the ones that have been sorted as highest
      values.

   if is null

      then behaviour is the same as for value < 0

  @return true

    $idScorePairListRef has been sorted (if it was unsorted).

  @return false

    no sorting took place, ie. when maxHighestValues == 0 or
    $idScorePairListRef === null. $idScorePairListRef remains as-is.

  @todo

    if $maxHighestValues is set to a a value > 0, sorting can be stopped
    once the number of sorted elements reaches $maxHighestValues. all
    other lower values will be neglecetd anyways.
*/
function sort_relationship_score_highest_to_lowest(array &$idScorePairListRef, int $maxHighestValues = 10): bool {

  if ( $maxHighestValues == 0 || $idScorePairListRef === null ) return false;

  usort($idScorePairListRef, reversePairSortOp(...));

  if ( $maxHighestValues === null ) $maxHighestValues = -1;
  if($maxHighestValues > 0 && count($idScorePairListRef) > $maxHighestValues) {
    $idScorePairListRef = array_slice($idScorePairListRef, 0, $maxHighestValues);
  }
  return true;
}

?>
