<?php

use PHPUnit\Framework\TestCase;

require_once 'challengetwo_pair.php';

class test_challengetwo_pair extends TestCase {

  public function testPair() {
    $pair = new Pair(1, 2);
    $this->assertEquals($pair->first, 1);
    $this->assertEquals($pair->second, 2);

    $pair = new Pair("a", "b");
    $this->assertEquals($pair->first, "a");
    $this->assertEquals($pair->second, "b");

    $pair = new Pair(1, "b");
    $this->assertEquals($pair->first, 1);
    $this->assertEquals($pair->second, "b");
  }

  public function testPairNull() {
    $pair = new Pair(1, null);
    $this->assertEquals($pair->first, 1);
    $this->assertNull($pair->second);

    $pair = new Pair(null, "b");
    $this->assertNull($pair->first);
    $this->assertEquals($pair->second, "b");

    $pair = new Pair(null, null);
    $this->assertNull($pair->first);
    $this->assertNull($pair->second);
  }

  public function testPairCloneDeep() {
    $pair = new Pair(1, 2);
    $pairClone = clone $pair;

    $this->assertEquals($pairClone->first, 1);
    $this->assertEquals($pairClone->second, 2);

    $pairClone->first = "a";
    $pairClone->second = "b";

    $this->assertEquals($pair->first, 1);
    $this->assertEquals($pair->second, 2);
    $this->assertEquals($pairClone->first, "a");
    $this->assertEquals($pairClone->second, "b");

    $pair = new Pair(array(1, 2, 3), array("a", "b", "c"));
    $pairClone = clone $pair;

    $this->assertEquals($pairClone->first[0], 1);
    $this->assertEquals($pairClone->first[2], 3);
    $this->assertEquals($pairClone->second[0], "a");
    $this->assertEquals($pairClone->second[2], "c");

    $pairClone->first = "a";
    $pairClone->second = "b";

    $this->assertEquals($pair->first[0], 1);
    $this->assertEquals($pair->second[0], "a");

    $this->assertEquals($pairClone->first, "a");
    $this->assertEquals($pairClone->second, "b");
  }

  // public function testPairCloneShallow() {}
}

?>
