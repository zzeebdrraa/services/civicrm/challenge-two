<?php

use PHPUnit\Framework\TestCase;

require_once 'challengetwo_output.php';

class test_challengetwo_output extends TestCase {

  protected $contactNameMap = array(
    "1"  => "name A",
    "3"  => "name C",
    "2"  => "name B",
    "4"  => "name D",
    "5"  => "name E",
    "10" => "name J",
    "6"  => "name F",
    "7"  => "name G",
    "8"  => "name H",
    "9"  => "name I"
  );

  protected $expectedOutputListDefault = array(
    "name J [3]",
    "name F [3]",
    "name A [2.9999]",
    "name B [2.5]",
    "name I [2]",
    "name G [2]",
    "name C [1.666]",
    "name E [1.654]",
    "name D [1]",
    "name H [0]"
  );

  protected $expectedOutputListWithNullDefault = array(
    "name J [3]",
    "name A [2.9999]",
    "name I [2]",
    "name C [1.666]",
    "name D [1]",
  );

  protected $expectedOutputListCustom = array(
    "name J - 3",
    "name F - 3",
    "name A - 2.9999",
    "name B - 2.5",
    "name I - 2",
    "name G - 2",
    "name C - 1.666",
    "name E - 1.654",
    "name D - 1",
    "name H - 0"
  );

  public function idScoreListDataProvider() {
    return [
      [ array(
          new Pair("10", 3),
          new Pair("6", 3),
          new Pair("1", 2.9999),
          new Pair("2", 2.5),
          new Pair("9", 2),
          new Pair("7", 2),
          new Pair("3", 1.666),
          new Pair("5", 1.654),
          new Pair("4", 1),
          new Pair("8", 0)
        )
      ]
    ];
  }

  public function idScoreListNullPairsDataProvider() {
    return [
      [ array(
          new Pair("10", 3),
          null,
          new Pair("1", 2.9999),
          null,
          new Pair("9", 2),
          null,
          new Pair("3", 1.666),
          null,
          new Pair("4", 1),
          null
        )
      ]
    ];
  }

  public function idScoreListNullPairMembersDataProvider() {
    return [
      [ array(
          new Pair(null, 3),
          new Pair("6", null),
          new Pair(null, null),
        )
      ]
    ];
  }

  /**
  * @test
  * @dataProvider idScoreListDataProvider
  */
  public function testGenerateOutputDefaultGenerator(array &$idScorePairList) {
    $this->assertNotNull($idScorePairList);
    $this->assertIsArray($idScorePairList);

    $outputList = generate_output_list($this->contactNameMap, $idScorePairList);

    $this->assertEquals(count($this->expectedOutputListDefault), count($outputList));
    $this->assertSame($this->expectedOutputListDefault, $outputList);
  }

  /**
  * @test
  * @dataProvider idScoreListDataProvider
  */
  public function testGenerateOutputCustomGenerator(array &$idScorePairList) {
    $this->assertNotNull($idScorePairList);
    $this->assertIsArray($idScorePairList);

    $customGenerator = function($name, $score) {
      return $name . " - " . $score;
    };
    $outputList = generate_output_list($this->contactNameMap, $idScorePairList, $customGenerator(...));

    $this->assertEquals(count($this->expectedOutputListCustom), count($outputList));
    $this->assertSame($this->expectedOutputListCustom, $outputList);
  }

  /**
  * @test
  * @dataProvider idScoreListNullPairsDataProvider
  */
  public function testGenerateOutputDefaultGeneratorNullPairs(array &$idScorePairList) {
    $this->assertNotNull($idScorePairList);
    $this->assertIsArray($idScorePairList);

    $outputList = generate_output_list($this->contactNameMap, $idScorePairList);

    $this->assertEquals(count($this->expectedOutputListWithNullDefault), count($outputList));
    $this->assertSame($this->expectedOutputListWithNullDefault, $outputList);
  }

  /**
  * @test
  * @dataProvider idScoreListNullPairMembersDataProvider
  */
  public function testGenerateOutputDefaultGeneratorNullPairMembers(array &$idScorePairList) {
    $this->assertNotNull($idScorePairList);
    $this->assertIsArray($idScorePairList);

    $outputList = generate_output_list($this->contactNameMap, $idScorePairList);
    $this->assertEquals(0, count($outputList));
  }

}

?>
