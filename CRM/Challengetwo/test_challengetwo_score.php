<?php

use PHPUnit\Framework\TestCase;

require_once 'challengetwo_score.php';
require_once 'challengetwo_assertions.php';

class ScoreTestDataBroken{

  public array $expectedIdScorePairListBrokenDepth10;
  public array $expectedIdScorePairListBrokenDepth03;

  public function __construct() {
    $this->expectedIdScorePairListBrokenDepth03 = array(
      new Pair(1, 0.0),
      new Pair(5, 0.0),
      new Pair(2, 0.0),
      new Pair(3, 1.0),
      new Pair(4, 1.0),
    );

    $this->expectedIdScorePairListBrokenDepth10 = array(
      new Pair(1, 0.0),
      new Pair(5, 0.0),
      new Pair(2, 0.0),
      new Pair(3, 1.0),
      new Pair(4, 1.0),
    );
  }
}

class ScoreTestDataIncomplete{

  public array $expectedIdScorePairListPartlyCompleteDepth10;
  public array $expectedIdScorePairListPartlyCompleteDepth03;

  public function __construct() {
    $this->expectedIdScorePairListPartlyCompleteDepth03 = array(
      new Pair(1, 0.0),
      new Pair(2, 1.0),
      new Pair(3, 0.0),
      new Pair(4, 1.0),
      new Pair(5, 0.0),
      new Pair(6, 1.8333333333333333),
      new Pair(7, 1.5),
      new Pair(8, 1.0)
    );

    $this->expectedIdScorePairListPartlyCompleteDepth10 = array(
      new Pair(1, 0.0),
      new Pair(2, 1.0),
      new Pair(3, 0.0),
      new Pair(4, 1.0),
      new Pair(5, 0.0),
      new Pair(6, 1.8333333333333333),
      new Pair(7, 1.5),
      new Pair(8, 1.0)
    );
  }
}

class ScoreTestDataInfinite{

  public array $expectedIdScorePairListSameWeightDepth10;
  public array $expectedIdScorePairListSameWeightDepth03;

  public function __construct() {
    $this->expectedIdScorePairListSameWeightDepth03 = array(
      new Pair(1, 1.8333333333333333),
      new Pair(3, 1.8333333333333333),
      new Pair(2, 1.8333333333333333),
      new Pair(4, 1.8333333333333333),
      new Pair(5, 1.8333333333333333),
      new Pair(6, 1.8333333333333333),
      new Pair(7, 1.8333333333333333),
      new Pair(8, 1.8333333333333333),
      new Pair(9, 1.8333333333333333)
    );

    $this->expectedIdScorePairListSameWeightDepth10 = array(
      new Pair(1, 2.8289682539682537),
      new Pair(3, 2.8289682539682537),
      new Pair(2, 2.8289682539682537),
      new Pair(4, 2.8289682539682537),
      new Pair(5, 2.8289682539682537),
      new Pair(6, 2.8289682539682537),
      new Pair(7, 2.8289682539682537),
      new Pair(8, 2.8289682539682537),
      new Pair(9, 2.8289682539682537)
    );
  }
}

class test_challengetwo_score extends TestCase {

  protected $contactNameMap = array(
    "1" => "name A",
    "3" => "name B",
    "2" => "name C",
    "4" => "name D",
    "5" => "name E",
    "2" => "name F",
    "6" => "name G",
    "7" => "name H",
    "8" => "name I",
    "9" => "name J"
  );

  protected $relMapInfinite = array(
    "1" => "2",
    "3" => "4",
    "2" => "3",
    "4" => "5",
    "5" => "6",
    "2" => "3",
    "6" => "7",
    "7" => "8",
    "8" => "9",
    "9" => "1"
  );

  protected $relMapPartlyComplete = array(
    "1" => "",
    "2" => "1",
    "3" => "",
    "4" => "1",
    "5" => "",
    "6" => "7",
    "7" => "8",
    "8" => "3"
  );

  /*
    "1" => "1"  => causes infinte loop
    "2" => "10" => invalid
    "5" => "5"  => dublicates
  */
  protected $relMapBroken = array(
    "1" => "1",
    "5" => "5",
    "5" => "5",
    "2" => "10",
    "3" => "2",
    "4" => "1",
  );

  protected $relMapNull = array(
    "1"  => null,
    null => null
  );

  public function relMapDepthInfiniteDataProvider() {
    $testData = new ScoreTestDataInfinite;

    return [
      [ $this->relMapInfinite,  10, $testData->expectedIdScorePairListSameWeightDepth10 ],
      [ $this->relMapInfinite, -1,  $testData->expectedIdScorePairListSameWeightDepth10 ],
      [ $this->relMapInfinite, -2,  $testData->expectedIdScorePairListSameWeightDepth10 ],
      [ $this->relMapInfinite,  3,  $testData->expectedIdScorePairListSameWeightDepth03 ]
    ];
  }

  public function relMapDepthIncompleteDataProvider() {
    $testData = new ScoreTestDataIncomplete;

    return [
      [ $this->relMapPartlyComplete,  10, $testData->expectedIdScorePairListPartlyCompleteDepth10 ],
      [ $this->relMapPartlyComplete, -1,  $testData->expectedIdScorePairListPartlyCompleteDepth10 ],
      [ $this->relMapPartlyComplete, -2,  $testData->expectedIdScorePairListPartlyCompleteDepth10 ],
      [ $this->relMapPartlyComplete,  3,  $testData->expectedIdScorePairListPartlyCompleteDepth03 ]
    ];
  }

  public function relMapDepthBrokenDataProvider() {
    $testData = new ScoreTestDataBroken;

    return [
      [ $this->relMapBroken,  10, $testData->expectedIdScorePairListBrokenDepth10 ],
      [ $this->relMapBroken, -1,  $testData->expectedIdScorePairListBrokenDepth10 ],
      [ $this->relMapBroken, -2,  $testData->expectedIdScorePairListBrokenDepth10 ],
      [ $this->relMapBroken,  3,  $testData->expectedIdScorePairListBrokenDepth03 ]
    ];
  }

  public function relMapDepthNullDataProvider() {
    return [
      [ $this->relMapNull, -1 ],
      [ $this->relMapNull,  10 ]
    ];
  }

  public function relMapDataProvider() {
    return [
      [ $this->relMapInfinite ],
      [ $this->relMapPartlyComplete ],
      [ $this->relMapBroken ],
      [ $this->relMapNull ]
    ];
  }

  /**
  * @test
  * @dataProvider relMapDepthInfiniteDataProvider
  */
  public function testCalculateRelationshipScoreInfinite($relMap, $depth, $expectedIdScorePairList) {
    $this->assertIsArray($relMap);
    $this->assertIsArray($expectedIdScorePairList);

    $idScorePairList = & calculate_relationship_score($relMap, $depth);
    PairAssert::assertSamePairList($expectedIdScorePairList, $idScorePairList);
  }

  /**
  * @test
  * @dataProvider relMapDepthIncompleteDataProvider
  */
  public function testCalculateRelationshipScoreIncomplete($relMap, $depth, $expectedIdScorePairList) {
    $this->assertIsArray($relMap);
    $this->assertIsArray($expectedIdScorePairList);

    $idScorePairList = & calculate_relationship_score($relMap, $depth);
    PairAssert::assertSamePairList($expectedIdScorePairList, $idScorePairList);
  }

  /**
  * @test
  * @dataProvider relMapDepthBrokenDataProvider
  */
  public function testCalculateRelationshipScoreBroken($relMap, $depth, $expectedIdScorePairList) {
    $this->assertIsArray($relMap);
    $this->assertIsArray($expectedIdScorePairList);

    $idScorePairList = & calculate_relationship_score($relMap, $depth);
    PairAssert::assertSamePairList($expectedIdScorePairList, $idScorePairList);
  }

  /**
  * @test
  * @dataProvider relMapDepthNullDataProvider
  */
  public function testCalculateRelationshipScoreNull($relMap, $depth) {
    $this->assertIsArray($relMap);

    $idScorePairList = & calculate_relationship_score($relMap, $depth);
    $this->assertEquals(0, count($idScorePairList));
  }

  /**
  * @test
  * @dataProvider relMapDataProvider
  */
  public function testCalculateRelationshipScoreZeroDepth($relMap) {
    $this->assertIsArray($relMap);

    $idScorePairList = calculate_relationship_score($relMap, 0);
    $this->assertEquals(0, count($idScorePairList));
  }
}
?>
