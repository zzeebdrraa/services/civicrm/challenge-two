?php

// TODO log and query functions should be factored out. then this function could be tested.
function query-individual-contacts-api-v3(&$contactsRelMapRef, &$contactsNameMap) {
  $result = civicrm_api3('Contact', 'get', [
    'sequential' => 1,
    'return' => ["id", "display_name"],
  ]);

  if ( $result['is_error'] == 1 ) {
    Civi::log()->error('could not query contacts: ' . $result['error_message']);
    return false;
  }

  $entryList = $result['values'];
  for($i = 0; $i < $result['count']; ++$i) {
    $entry = $entryList[$i];
    if ( $entry['contact_is_deleted') == '0' ) {
      $id = $entry['contact_id'];
      $name = $entry['display_name'];
      $contactsNameMap[$id] = $name;
    }
  }

  $result = civicrm_api3('Relationship', 'get', [
    'sequential' => 1,
    'return' => ["contact_id_a", "contact_id_b"],
  ]);

  if ( $result['is_error'] == 1 ) {
    Civi::log()->error('could not query relationships: ' . $result['error_message']);
    return false;
  }

  $entryList = $result['values'];
  for($i = 0; $i < $result['count']; ++$i) {
    $entry = $entryList[$i];
      $idA = $entry['contact_id_a'];
      $idB = $entry['contact_id_b'];
      $contactsRelMapRef[$idA] = $idB;
    }
  }

  return true;
}

?>
