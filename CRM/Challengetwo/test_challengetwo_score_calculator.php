<?php

use PHPUnit\Framework\TestCase;

require_once 'challengetwo_score.php';

class test_challengetwo_score_calculator extends TestCase {

  public function harmonicNumberDataProvider() {
    // partial-sum fraction expected harmonic number
    return [
      [1,           1,        2],
      [1,           4,        1.25],
      [1,           0,        1],
      [10,          0,       10],
      [1,          -1,        0],
      [1.5,         3,        1.8333333333333333],
      [1.5,         3.33,     1.8333333333333333]	// 3.33 will be rounded to 3 as a int is expected
    ];
  }

  /**
  * @test
  * @dataProvider harmonicNumberDataProvider
  */
  public function testCalculateHarmonicNumberOp($score, $depth, $expectedNewScore) {
    $result = calculateHarmonicNumberOp($score, $depth);
    $this->assertEquals($expectedNewScore, $result);
  }
}

?>
