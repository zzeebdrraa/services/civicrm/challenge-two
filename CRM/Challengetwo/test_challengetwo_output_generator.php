<?php

use PHPUnit\Framework\TestCase;

require_once 'challengetwo_output.php';

class test_challengetwo_output_generator extends TestCase {

  public function nameScoreDataProvider() {
    return [
      ['abc', '123', 'abc [123]'],
      ['abc', 123,   'abc [123]'],
      ['abc', '',    'abc [<unknown>]'],
      ['',    123,   '<unknown> [123]'],
      ['',    '',    '<unknown> [<unknown>]'],
      ['abc', null,  'abc [<unknown>]'],
      [null,  123,   '<unknown> [123]'],
      [null,  null,  '<unknown> [<unknown>]']
    ];
  }

  public function nameScorePlaceholderAsStringDataProvider() {
    return [
      ['abc', '',    '/na', 'abc [/na]'],
      ['',    123,   '/na', '/na [123]'],
      ['',    '',    '/na', '/na [/na]'],
      ['abc', null,  '/na', 'abc [/na]'],
      [null,  123,   '/na', '/na [123]'],
      [null,  null,  '/na', '/na [/na]']
    ];
  }

  public function nameScorePlaceholderAsIntDataProvider() {
    return [
      ['abc', '',    -1, 'abc [-1]'],
      ['',    123,   -1, '-1 [123]'],
      ['',    '',    -1, '-1 [-1]'],
      ['abc', null,  -1, 'abc [-1]'],
      [null,  123,   -1, '-1 [123]'],
      [null,  null,  -1, '-1 [-1]']
    ];
  }

  public function nameScorePlaceholderAsFloatDataProvider() {
    return [
      ['abc', '',    -1.1, 'abc [-1.1]'],
      ['',    123,   -1.1, '-1.1 [123]'],
      ['',    '',    -1.1, '-1.1 [-1.1]'],
      ['abc', null,  -1.1, 'abc [-1.1]'],
      [null,  123,   -1.1, '-1.1 [123]'],
      [null,  null,  -1.1, '-1.1 [-1.1]']
    ];
  }

  /**
  * @test
  * @dataProvider nameScoreDataProvider
  */
  public function testGenerateOutputOpDefaultPlaceholder($name, $score, $expectedStr) {
    $outStr = generateNameScoreStringOp($name, $score);
    $this->assertEquals($expectedStr, $outStr);
  }

  /**
  * @test
  * @dataProvider nameScorePlaceholderAsStringDataProvider
  * @dataProvider nameScorePlaceholderAsIntDataProvider
  * @dataProvider nameScorePlaceholderAsFloatDataProvider
  */
  public function testGenerateOutputOpCustomPlaceholderAsString($name, $score, $placeholder, $expectedStr) {
    $outStr = generateNameScoreStringOp($name, $score, $placeholder);
    $this->assertEquals($expectedStr, $outStr);
  }
}
?>
