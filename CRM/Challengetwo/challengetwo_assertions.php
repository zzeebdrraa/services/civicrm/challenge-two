<?php

use PHPUnit\Framework\Assert;

class PairAssert {

  public static function assertSamePairList(array $aExpectedList, array $aGivenList): void {
    Assert::AssertEquals(count($aExpectedList), count($aGivenList));

    foreach ($aExpectedList as $index => $pairExpected) {
      Assert::assertInstanceOf(Pair::class, $pairExpected);
      Assert::assertInstanceOf(Pair::class, $aGivenList[$index]);

      Assert::AssertEquals($pairExpected->first, $aGivenList[$index]->first);
      Assert::AssertEquals($pairExpected->second, $aGivenList[$index]->second);
    }
  }
}

?>
