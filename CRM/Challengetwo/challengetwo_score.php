<?php

require_once 'challengetwo_pair.php';

/* calculates and return the next harmonic-number in a harmonic series as
   floating point number.

   @note

     if $fraction == 0, then $partialSum remains as-is.

   @example

     if function is being called for iteration steps from 1 to 5 and initial
     partial sum is 0.0, then the final harmonuc number is calculated as:

       0.0 + 1/1 + 1/2 + 1/3 + 1/4 + 1/5 => 2.283

   @seealso

    https://en.wikipedia.org/wiki/Harmonic_series_(mathematics)
*/
function calculateHarmonicNumberOp(float $partialSum, int $fraction): float {
  if ( $fraction == 0 ) return $partialSum;
  return $partialSum + (1 / $fraction);
}

/*
  function calculates a score that represents the depth of key relations in
  $keyRelMapRef argument.

  the depth of a key relation indicates for how many levels the value of a
  particular key represents another key in the map.

  the score is either calculated by a given $calcScoreOp or as default by
  calculateRelationScoreSimpleOp().

  @note

    this function is protected against infinite loops. it detects if a subkey
    has been visited already when iterating over the subkeys of a particular key.

    this might either be the case, when the value of a key represents its own key
    value:

      array(
        "1" => "1",  // value refers to its own key "1"
        ...
      )

    or if a chain of subkeys finally points to the initial main key

      array(
        "1" => "1",
        "2" => "3",
        "3" => "1"
      )

  @param $keyRelMapRef

    the key-values in this map ressemble a key-key relationship.

    - a value can be an already existing key in the map.
    - a value can point to its own key
    - a value can also refer to non-existing key or can be empty/null

    the map might look like this:

      array(
        "1" => "2",  // value refers to key "2"
        "2" => "3",  // value refers to key "3"
        "3" => "1",  // value refers to key "1"
        "4" => "4",  // value refers to itself
        "5" => "10", // value refers to a non-existing key "10"
        "6" => ""    // value is empty
        "7" => null  // value is null
      )

  @param $maxDepth

    if set to a value > 0:

      the score of a particular key is only calculated until the number
      of iterations that follow its subkeys within the map has reached
      $maxDepth (if that many iterations are possible for a key at all).

    if set to 0:

      score wont be calculated for any key in $keyRelMapRef

    if is null:

      $maxDepth has no effect

    if set to a value < 0:

      $maxDepth has no effect

    default value [10]

  @param $calcScoreOp

    a function that calculates the score.

    if not set, the default calculateHarmonicNumberOp() will be used.

    if a custom function should be used, then function signature is expected
    to look like this:

    - myfun(float $currentScore, int $currentDepth): float
    - the return value is the new score

  @return $keyScorePairList

    the result of this function is a list of value pairs:

    - the first value of a pair is a key of map parameter $keyRelMapRef
    - the second value of a pair is the calculated score as floating point
      number.

    the returned list might looks like this:

      array(
        Pair("1", 1.0),
        Pair("2", 4.5),
        ...
      )

   if $keyRelMapRef argument is null

   - $keyScorePairList is an empty array

   if $maxDepth is 0

   - $keyScorePairList is an empty array

   otherwise

   - $keyScorePairList contains as many entries as the number of
     elements in $keyRelMapRef
*/
function &calculate_relationship_score(array &$keyRelMapRef, int $maxDepth = 10, callable $callableCalcScoreOp = null): array {
  static $keyScorePairList;
  $keyScorePairList = array();

  if ( $maxDepth == 0 ) return $keyScorePairList;
  if ( $keyRelMapRef === null ) return $keyScorePairList;
  if ( $callableCalcScoreOp === null ) $callableCalcScoreOp = calculateHarmonicNumberOp(...);

  foreach ($keyRelMapRef as $mainKey=>$nextKey) {
    if( $mainKey === null || $nextKey === null ) continue;

    $score = 0.0;
    $depth = 1;
    $currentKey = $mainKey;
    $keysVisitedSet = array();

    while ( $maxDepth < 0 || $depth <= $maxDepth ) {
      if ( $nextKey == $currentKey ) break;

      $currentKey = $nextKey;
      if ( array_key_exists($currentKey, $keysVisitedSet) ) break;

      $nextKey = $keyRelMapRef[$currentKey] ?? null;
      if ( $nextKey === null ) break;

      $keysVisitedSet[$currentKey] = true;
      $score = $callableCalcScoreOp($score, $depth);
      $depth++;
    }
    array_push($keyScorePairList, new Pair($mainKey,$score));
  }
  return $keyScorePairList;
}

?>
