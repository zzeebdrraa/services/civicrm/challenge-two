<?php

require_once 'challengetwo.civix.php';
// phpcs:disable
use CRM_Challengetwo_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function challengetwo_civicrm_config(&$config): void {
  _challengetwo_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function challengetwo_civicrm_install(): void {
  _challengetwo_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function challengetwo_civicrm_enable(): void {
  _challengetwo_civix_civicrm_enable();
}
